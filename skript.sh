#!/bin/bash
clear
getretval() {
    case $retval in
    0)
        true
        ;;
    1)
        clear
        exit 1
        ;;
    255)
        clear
        exit 1
        ;;
    esac
}

leiasshconf() {
    clear
    if [ -f "/etc/ssh/sshd_config" ]; then
        sshconf="/etc/ssh/sshd_config"
    else
        vbconfigssh="$(find / -name "sshd_config" 2>/dev/null)"
        while IFS= read -r line; do
            dialog --clear --backtitle "
          Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --yesno "Faili sshd_config ei leitud! Kas $line sobib?" 7 50

            vastus=$?
            case $vastus in
            0)
                sshconf="$line"
                break
                ;;
            esac
        done <<<"$vbconfigssh"
        if [ -z "$sshconf" ]; then
            clear
            exit 1
        fi
        clear
    fi
}

fail2ban() {
    leiasshconf
    if command -v firewall-cmd >/dev/null 2>&1 && [ "$distro" = "centos" ]; then
        echo "Fail2bani installimine!"
        yum -y -q -e 0 install epel-release
        yum -y -q -e 0 install fail2ban-firewalld
        systemctl enable fail2ban
        systemctl restart fail2ban
        banaction="firewallcmd-ipset"
        logpath="/var/log/secure"
        echo "Fail2ban installitud!"
    elif [ "$distro" = "centos" ]; then
        echo "Fail2bani installimine!"
        yum -y -q -e 0 install epel-release
        yum -y -q -e 0 install fail2ban
        systemctl enable fail2ban
        systemctl restart fail2ban
        banaction="iptables-multiport"
        logpath="/var/log/secure"
        echo "Fail2ban installitud!"
    else
        echo "Fail2bani installimine!"
        apt -y -qq install fail2ban >/dev/null
        systemctl enable fail2ban
        systemctl restart fail2ban
        banaction="iptables-multiport"
        logpath="/var/log/auth.log"
        echo "Fail2ban installitud!"
    fi
    if grep -q "^Port" "$sshconf"; then
        sshport=$(grep "^Port" "$sshconf" | grep -o '[0-9]*')
    else
        sshport="22"
    fi
    echo "Config faili loomine"
    cat <<EOF >/etc/fail2ban/jail.local
[DEFAULT]
bantime = 3600
findtime = 600
banaction = $banaction
maxretry = 4

[ssh]
enabled = true
port = $sshport
filter = sshd
logpath = $logpath
maxretry = 4
EOF
    systemctl restart fail2ban
    echo "Fail2ban installitud!"
    read -n 1 -s -r -p "Vajutage ENTER et jätkata!"
    clear
}

kellelessh() {
    clear
    kordus=true
    while $kordus; do
        sshkasutaja="$(dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
            --title "TURVAMINE" \
            --inputbox "Millisele kasutajale sooviksite SSH võtmeid?" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        clear
        kordus=false
        if [[ $sshkasutaja =~ [[:space:]]+ ]] || [ -z $sshkasutaja ]; then
            dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "Kasutajanimes ei või olla tühikuid ja ei või olla tühi!" 10 30
            kordus=true
        elif ! id -u $sshkasutaja >/dev/null 2>&1; then
            dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "Kasutajat nimega $sshkasutaja ei eksisteeri!" 10 30
            kordus=true
        fi
    done

}

genvotmed() {
    clear
    kordus=true
    while $kordus; do
        sshparool="$(dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
            --title "TURVAMINE" \
            --inputbox "Palun sisestage parool SSH võtmete jaoks (vähemalt 5 tähemärki, võib jätta tühjaks)" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        clear
        kordus=false
        if [ ! -z "$sshparool" ] && [ "${#sshparool}" -lt "5" ]; then
            dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "Parool peab olema vähemalt 5 tähemärki!" 10 30
            kordus=true
        fi
    done
    ssh-keygen -t rsa -N "$sshparool" -f $kasutajahome/votmed.$$.rsa
    avalikssh="$(cat "$kasutajahome/votmed.$$.rsa.pub")"
}

sshvotmed() {
    clear
    dialog --clear --backtitle "
      Linuxi automatiseerimine Bash skriptitega" \
        --title "TURVAMINE" \
        --yesno "Kas soovite kasutajale $USER seadistada SSH võtmeid?" 10 30
    retval=$?
    case $retval in
    0)
        sshkasutaja="$USER"
        ;;
    1)
        kellelessh
        ;;
    255)
        clear
        exit 1
        ;;
    esac
    kasutajahome=$(getent passwd $sshkasutaja | awk -F ":" '{print $6}')
    clear
    kordus=true
    while $kordus; do
        avalikssh="$(dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
            --title "TURVAMINE" \
            --inputbox "Palun sisestage SSH avalik võti või vajutage Cancel et genereerida!" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        case $retval in
        0)
            true
            ;;
        1)
            genvotmed
            ;;
        255)
            clear
            exit 1
            ;;
        esac
        clear
        kordus=false
        if [ -z "$avalikssh" ]; then
            dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "SSH avalik võti ei või tühi olla!" 10 30
            kordus=true
        fi
    done
    mkdir -p $kasutajahome/.ssh
    echo "$avalikssh" >>$kasutajahome/.ssh/authorized_keys
    fail="$kasutajahome/$$.sshvotmed-$(date +'%d-%m-%Y')"
    echo "SSH VÕTMETE PAIGALDAMINE LÕPETATUD!" | tee -a $fail
    echo "Avalik võti on lisatud faili $kasutajahome/.ssh/authorized_keys" | tee -a $fail
    echo "Kasutaja: $sshkasutaja"
    echo "Avaliku võtme asukoht on $kasutajahome/votmed.$$.rsa.pub" | tee -a $fail
    echo "Privaatse võtme asukoht on $kasutajahome/votmed.$$.rsa." | tee -a $fail
    if [ ! -z "$sshparool" ]; then echo "Privaatse võtme parool: $sshparool" | tee -a $fail; fi
    echo "Kõik üleval olev info on kirjutatud faili $fail!"
    read -n 1 -s -r -p "Vajutage ENTER et jätkata!"
    clear
}

vahetaport() {
    leiasshconf
    if grep -q "^Port" "$sshconf"; then
        failis=jah
        vanaport=$(grep "^Port" "$sshconf" | grep -o '[0-9]*')
    else
        failis=ei
        vanaport="22"
    fi
    clear
    kordus=true
    while $kordus; do
        uusport="$(dialog --clear --backtitle "
  Linuxi automatiseerimine Bash skriptitega" \
            --title "TURVAMINE" \
            --inputbox "Palun sisestage uus port SSH jaoks (1024-65535)! (Praegune: $vanaport)" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        clear
        kordus=false
        if lsof -Pi :${uusport} -sTCP:LISTEN -t >/dev/null; then
            dialog --clear --backtitle "
Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "Port $uusport on juba kasutuses, palun valige muu port!" 10 30
            kordus=true
        elif [[ ! "$uusport" =~ ^[0-9]+$ ]] || [ ! "$uusport" -ge 1024 -a "$uusport" -le 65535 ]; then
            dialog --clear --backtitle "
  Linuxi automatiseerimine Bash skriptitega" \
                --title "TURVAMINE" \
                --msgbox "Port $uusport ei ole vahemikus 1024-65535, palun valige muu port!" 10 30
            kordus=true
        fi
    done
    if command -v ufw >/dev/null 2>&1; then
        ufw allow $uusport
    elif command -v firewall-cmd >/dev/null 2>&1; then
        firewall-cmd --add-port=${uusport}/tcp
        firewall-cmd --add-port=${uusport}/udp
        firewall-cmd --add-port=${uusport}/tcp --permanent
        firewall-cmd --add-port=${uusport}/udp --permanent
    elif command -v iptables >/dev/null 2>&1; then
        iptables -A INPUT -p tcp --dport ${uusport} -j ACCEPT
        if [ -f /etc/iptables/rules.v4 ]; then
            iptables-save >/etc/iptables/rules.v4
        fi
    fi
    if [ "$failis" = "jah" ]; then
        cp $sshconf $sshconf.bak
        sed -i "s/^Port .*/Port ${uusport}/" $sshconf
    else

        cp $sshconf $sshconf.bak
        sed -i "1iPort ${uusport}" $sshconf
    fi
    systemctl restart sshd
    echo "SKRIPT LÕPETATUD!"
    echo "Uus SSH port on ${uusport}"
    read -n 1 -s -r -p "Vajutage ENTER et jätkata!"
    clear
}

veebiserverlopp() {
    cat <<EOF >/srv/webvalmis
webvalmis
$(date +'%d/%m/%Y')
EOF
    fail="$HOME/install-$(date +'%d-%m-%Y')"
    echo "INSTALL LÕPETATUD!" | tee -a $fail
    if [ "$kasdomeen" = "IP" ]; then echo "AADRESS: $misip" | tee -a $fail; else echo "AADRESS: $misdomeen" | tee -a $fail; fi
    echo "Lehe asukoht on /var/www/leht!" | tee -a $fail
    if [ "$misphp" != "EiSooviPHPd" ]; then echo "PHP versioon: $misphp" | tee -a $fail; fi
    if [ "$missql" != "EiSooviSQLi" ]; then echo "SQL andmebaas: $missql" | tee -a $fail; fi
    if [ "$missql" != "EiSooviSQLi" ]; then echo "SQL kasutaja: root" | tee -a $fail; fi
    if [ "$missql" != "EiSooviSQLi" ]; then echo "SQL parool: $mysqlparool" | tee -a $fail; fi
    if [ "$mispma" = "Jah" ] && [ "$kasdomeen" = "IP" ]; then echo "phpMyAdmin aadress: $misip/pmaportaal" | tee -a $fail; fi
    if [ "$mispma" = "Jah" ] && [ "$kasdomeen" = "Domeen" ]; then echo "phpMyAdmin aadress: $misdomeen/pmaportaal" | tee -a $fail; fi
    echo "Kõik üleval olev info on kirjutatud faili $fail!"
    read -n 1 -s -r -p "Vajutage ENTER et jätkata!"
    clear
}

installipma() {
    if [ "$mispma" = "Jah" ]; then
        blowfishsecret="$(tr </dev/urandom -dc A-Za-z0-9_ | head -c 32)"
        # siin oleks saanud kasutada apt -y install phpmyadmin või yum -y install phpMyAdmin
        # aga need küsiksid küsimusi (v.a yum-i oma), see on universaalsem
        pma="$(curl -L -s https://www.phpmyadmin.net/home_page/version.txt)"
        pmaaadress="$(sed -n '3p' <<<"$pma")"
        pmaversioon="$(sed -n '1p' <<<"$pma")"
        wget -o /dev/null -q -P /tmp "$pmaaadress"
        if grep -q -e '.zip' <<<"$pmaaadress"; then
            if [ "$distro" = "centos" ]; then
                yum -y -q -e 0 install unzip
            else
                apt -y -qq install unzip
            fi
            unzip -qq /tmp/phpMyAdmin-*.zip -d /tmp/phpMyAdmin
            rm -f /tmp/phpMyAdmin-*.zip
        else
            tar -zxvf /tmp/phpMyAdmin-*.tar.gz -C /tmp/phpMyAdmin
            rm -f /tmp/phpMyAdmin-*.tar.gz
        fi
        mkdir -p /usr/share/phpmyadmin
        mkdir -p /usr/share/phpmyadmin/tmp
        mv /tmp/phpMyAdmin/phpMyAdmin-*/* /usr/share/phpmyadmin
        rm -r /tmp/phpMyAdmin
        cp -pr /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php
        sed -i "s:cfg\['blowfish_secret'\] = '':cfg['blowfish_secret'] = '${blowfishsecret}':" /usr/share/phpmyadmin/config.inc.php
        mysql </usr/share/phpmyadmin/sql/create_tables.sql -uroot -p${mysqlparool}
        if [ "$misveebiserver" = "NGINX" ] && [ "$distro" != "centos" ]; then
            chown -R www-data:www-data /usr/share/phpmyadmin
        elif [ "$misveebiserver" = "Apache" ]; then
            chown -R apache:apache /usr/share/phpmyadmin
        elif [ "$misveebiserver" = "NGINX" ] && [ "$distro" = "centos" ]; then
            chown -R nginx:nginx /usr/share/phpmyadmin
            chcon -Rt httpd_sys_content_t /usr/share/phpmyadmin
            chcon -Rt httpd_sys_rw_content_t "/usr/share/phpmyadmin/tmp(/.*)?"
            restorecon -Rv /usr/share/phpmyadmin
        fi
        ln -s /usr/share/phpmyadmin /var/www/leht/pmaportaal
        sleep 10
    fi
}

mysqlsetup() {
    if [ "$distro" = "debian" ] && [ "$missql" = "MySQL" ]; then
        sqlline="mysql -uroot -p$mysqlparool"
    else
        sqlline="mysql -uroot"
    fi
    if [ "$missql" = "MySQL" ]; then
        $sqlline <<EOF
USE mysql;
ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '${mysqlparool}';
DELETE FROM mysql.user WHERE user='root' AND host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE user='';
DROP DATABASE IF EXISTS test;
FLUSH PRIVILEGES;
EOF
    elif [ "$missql" = "MariaDB" ]; then
        $sqlline <<EOF
USE mysql;
ALTER USER 'root'@'localhost' IDENTIFIED BY '${mysqlparool}';
DELETE FROM mysql.global_priv WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE user='';
DROP DATABASE IF EXISTS test;
FLUSH PRIVILEGES;
EOF
    fi
}

installisql() {
    mysqlparool="$(tr </dev/urandom -dc A-Za-z0-9_ | head -c 20)"
    if [ "$missql" = "MySQL" ]; then
        if [ "$distro" = "ubuntu" ]; then
            echo "MySQLi installimine!"
            apt -y -qq update >/dev/null
            apt -y -qq install mysql-server >/dev/null
            mysqlsetup
            systemctl enable mysql
            echo "MySQL installitud!"
        elif [ "$distro" = "debian" ]; then
            echo "MySQLi installimine!"
            apt install -qq -y dirmngr gnupg >/dev/null
            apt-key adv --keyserver pool.sks-keyservers.net --recv-keys 5072E1F5
            echo "deb http://repo.mysql.com/apt/debian $(lsb_release -sc) mysql-8.0" | tee /etc/apt/sources.list.d/mysql80.list
            apt -qq -y update
            echo "mysql-community-server mysql-community-server/root-pass password $mysqlparool" | debconf-set-selections
            echo "mysql-community-server mysql-community-server/re-root-pass password $mysqlparool" | debconf-set-selections
            echo "mysql-community-server mysql-server/default-auth-override select Use Legacy Authentication Method (Retain MySQL 5.x Compatibility)" | debconf-set-selections
            DEBIAN_FRONTEND=noninteractive apt -qq -y install mysql-server >/dev/null
            systemctl restart mysql
            mysqlsetup
            systemctl restart mysql
            systemctl enable mysql
            echo "MySQL installitud!"
        elif [ "$distro" = "centos" ]; then
            if [ "$versioon" = "7" ]; then
                echo "MySQLi installimine!"
                yum -y -q -e 0 localinstall https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
                yum -y -q -e 0 install mysql-community-server
                systemctl restart mysql
                mysqlsetup
                systemctl enable mysqld
                systemctl restart mysqld
                echo "MySQL installitud!"
            elif [ "$versioon" = "8" ]; then
                echo "MySQLi installimine!"
                yum -y -q -e 0 install @mysql
                systemctl restart mysqld
                mysqlsetup
                systemctl enable mysqld
                systemctl restart mysqld
                echo "MySQL installitud!"
            fi
        fi
    elif [ "$missql" = "MariaDB" ]; then
        if [ "$distro" = "ubuntu" ] || [ "$distro" = "debian" ]; then
            echo "MariaDB installimine!"
            if [ "$distro" = "ubuntu" ]; then
                apt -y -qq install software-properties-common >/dev/null
            elif [ "$distro" = "debian" ]; then
                apt -y -qq install software-properties-common dirmngr >/dev/null
            fi
            apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
            add-apt-repository -y "deb [arch=amd64] http://mirror.netinch.com/pub/mariadb/repo/10.4/$distro $(lsb_release -sc) main"
            apt -qq -y update
            apt -qq -y install mariadb-server >/dev/null
            systemctl restart mariadb
            mysqlsetup
            systemctl enable mariadb
            systemctl restart mariadb
            echo "MariaDB installitud!"
        elif [ "$distro" = "centos" ]; then
            echo "MariaDB installimine!"
            cat <<EOF >/etc/yum.repos.d/MariaDB.repo
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/centos${versioon}-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF
            if [ "$versioon" = "7" ]; then
                yum -y -q -e 0 install MariaDB-server MariaDB-client
            elif [ "$versioon" = "8" ]; then
                dnf -y -q install boost-program-options
                dnf -y -q install MariaDB-server MariaDB-client --disablerepo=AppStream
            fi
            systemctl restart mariadb
            mysqlsetup
            systemctl enable mariadb
            systemctl restart mariadb
        fi
    fi
}
ubdebphp() {
    if [ "$distro" = "ubuntu" ]; then
        echo "PHP installimine!"
        apt -y -qq install software-properties-common >/dev/null
        add-apt-repository -y ppa:ondrej/php >/dev/null
        apt -y -qq update >/dev/null
        if [ "$misveebiserver" = "NGINX" ]; then
            apt install -qq -y php$misphp php$misphp-cli php$misphp-fpm php$misphp-curl php$misphp-gd php$misphp-mysql php$misphp-json php$misphp-pdo php$misphp-mbstring php$misphp-tokenizer php$misphp-zip >/dev/null
        elif [ "$misveebiserver" = "Apache" ]; then
            apt install -qq -y php$misphp php$misphp-cli php$misphp-fpm php$misphp-curl php$misphp-gd php$misphp-mysql php$misphp-pdo php$misphp-json php$misphp-mbstring php$misphp-tokenizer php$misphp-zip libapache2-mod-php$misphp >/dev/null
        fi
        echo "PHP installitud!"
        systemctl restart php$misphp-fpm
    elif [ "$distro" = "debian" ]; then
        echo "PHP installimine!"
        apt install -qq -y ca-certificates apt-transport-https gnupg >/dev/null
        wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
        echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
        apt -y -qq update >/dev/null
        if [ "$misveebiserver" = "NGINX" ]; then
            apt install -qq -y php$misphp php$misphp-cli php$misphp-fpm php$misphp-curl php$misphp-gd php$misphp-mysql php$misphp-json php$misphp-pdo php$misphp-mbstring php$misphp-zip >/dev/null
        elif [ "$misveebiserver" = "Apache" ]; then
            apt install -qq -y php$misphp php$misphp-cli php$misphp-fpm php$misphp-curl php$misphp-gd php$misphp-mysql php$misphp-pdo php$misphp-json php$misphp-mbstring php$misphp-zip libapache2-mod-php$misphp
        fi
        echo "PHP installitud!"
        systemctl restart php$misphp-fpm
    fi
}

centosphpinstall() {
    centosphp=$(echo "${misphp//./}")
    if [ "$versioon" = "7" ]; then
        echo "PHP installimine!"
        yum -y -q -e 0 install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        yum -y -q -e 0 install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
        yum -y -q -e 0 install yum-utils
        yum update -q -y
        yum-config-manager --enable remi-php${centosphp}
        yum install -q -e 0 -y php php-common php-fpm php-cli php-json php-mysqlnd php-gd php-mbstring php-pdo php-zip
        echo "PHP installitud!"
    elif [ "$versioon" = "8" ]; then
        echo "PHP installimine!"
        dnf -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
        dnf -q -y install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
        dnf -q -y install yum-utils
        dnf -y module reset php
        dnf -y module install php:remi-${misphp}
        dnf -y -q update
        dnf install -q -y php php-common php-fpm php-cli php-json php-mysqlnd php-gd php-mbstring php-pdo php-zip
        echo "PHP installitud!"
    fi
}

installiveebiserver() {
    clear
    if [ "$misveebiserver" = "NGINX" ]; then
        mkdir -p /var/www/leht
        if [ "$distro" = "ubuntu" ] || [ "$distro" = "debian" ]; then
            ufw allow 80
            ufw allow 443
            echo "apt uuendamine"
            apt -y -qq update >/dev/null
            echo "NGINXi installimine"
            apt -qq -y install nginx >/dev/null
            echo "NGINX installitud!"
            systemctl restart nginx
            systemctl enable nginx
            if [ "$kasdomeen" = "IP" ]; then
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/nginx/sites-available/leht.conf
server_tokens off;

server {
    listen 80;
    listen [::]:80;
    server_name ${misip};

    root /var/www/leht;
    index index.html index.htm;
    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.html;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        log_not_found off;
        access_log off;
    }

    location ~ /\.(?!well-known) {
        deny all;
    }
}
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/nginx/sites-available/leht.conf /etc/nginx/sites-enabled/leht.conf
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                else
                    ubdebphp
                    cat <<EOF >/etc/nginx/sites-available/leht.conf
server_tokens off;

server {
    listen 80;
    listen [::]:80;
    server_name ${misip};

    root /var/www/leht;
    index index.php index.html index.htm;
    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.php;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        log_not_found off;
        access_log off;
    }

    location ~ \.php\$ {
        fastcgi_pass unix:/run/php/php${misphp}-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
        include /etc/nginx/fastcgi_params;
    }

    location ~ /\.(?!well-known) {
        deny all;
    }
}
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/nginx/sites-available/leht.conf /etc/nginx/sites-enabled/leht.conf
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                fi

            elif [ "$kasdomeen" = "Domeen" ]; then
                if [ "$distro" = "ubuntu" ]; then
                    echo "certboti installimine"
                    apt -y -qq install software-properties-common >/dev/null
                    add-apt-repository -y universe >/dev/null
                    add-apt-repository -y ppa:certbot/certbot >/dev/null
                    apt -y -qq update >/dev/null
                    apt -y -qq install certbot python-certbot-nginx >/dev/null
                    echo "certbot installitud!"
                elif [ "$distro" = "debian" ]; then
                    echo "certboti installimine"
                    apt -y -qq install certbot python-certbot-nginx >/dev/null
                    echo "certbot installitud!"
                fi
                echo "SSL sertifikaadi genereerimine!"
                systemctl restart nginx
                certbot certonly --nginx --non-interactive --agree-tos --domains ${misdomeen} --register-unsafely-without-email
                echo "SSL sertifikaat genereeritud!"
                systemctl restart nginx
                crontab -l | {
                    cat
                    echo "43 6 * * * certbot renew --post-hook "systemctl reload nginx""
                } | crontab -
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/nginx/sites-available/leht.conf
server_tokens off;

server {
    listen 80;
    server_name ${misdomeen} www.${misdomeen};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl http2;
    server_name ${misdomeen};

    root /var/www/leht;
    index index.php index.html index.htm;
    charset utf-8;

    ssl_session_cache shared:SSL:50m;
    ssl_session_timeout 1d;
    ssl_session_tickets off;

    ssl_certificate /etc/letsencrypt/live/${misdomeen}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${misdomeen}/privkey.pem;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    ssl_protocols TLSv1.2;

    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";



    location / {
        try_files \$uri \$uri/ /index.php;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        log_not_found off;
        access_log off;
    }

    location ~ /\.(?!well-known) {
        deny all;
    }
}
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/nginx/sites-available/leht.conf /etc/nginx/sites-enabled/leht.conf
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                else
                    ubdebphp
                    cat <<EOF >/etc/nginx/sites-available/leht.conf
server_tokens off;

server {
    listen 80;
    server_name ${misdomeen} www.${misdomeen};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl http2;
    server_name ${misdomeen};

    root /var/www/leht;
    index index.php index.html index.htm;
    charset utf-8;

    ssl_session_cache shared:SSL:50m;
    ssl_session_timeout 1d;
    ssl_session_tickets off;

    ssl_certificate /etc/letsencrypt/live/${misdomeen}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${misdomeen}/privkey.pem;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    ssl_protocols TLSv1.2;

    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";



    location / {
        try_files \$uri \$uri/ /index.php;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        log_not_found off;
        access_log off;
    }

    location ~ \.php\$ {
        fastcgi_pass unix:/run/php/php${misphp}-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
        include /etc/nginx/fastcgi_params;
    }

    location ~ /\.(?!well-known) {
        deny all;
    }
}
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/nginx/sites-available/leht.conf /etc/nginx/sites-enabled/leht.conf
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                fi
                systemctl restart nginx
            fi
        elif [ "$distro" = "centos" ]; then
            if command -v firewall-cmd >/dev/null 2>&1; then
                firewall-cmd --add-port=80/tcp
                firewall-cmd --add-port=443/tcp
                firewall-cmd --add-port=80/tcp --permanent
                firewall-cmd --add-port=443/tcp --permanent
            fi
            chcon -Rt httpd_sys_content_t /var/www/leht
            echo "NGINXi installimine!"
            if [ "$versioon" = "7" ]; then
                yum -y -q -e 0 install epel-release
            fi
            yum -y -q -e 0 install nginx
            echo "NGINX installitud!"
            if [ "$kasdomeen" = "IP" ]; then
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
  listen 80;
  listen [::]:80;
  server_name $misip;

  root /var/www/leht;
  index index.html index.htm;
  charset utf-8;

  location / {
      try_files \$uri \$uri/ /index.html;
  }

  location = /favicon.ico {
      log_not_found off;
      access_log off;
  }

  location = /robots.txt {
      log_not_found off;
      access_log off;
  }

  location ~ /\.(?!well-known) {
      deny all;
  }
}
EOF
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                else
                    centosphpinstall
                    if [ "$versioon" = "7" ]; then
                        cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
  listen 80;
  listen [::]:80;
  server_name $misip;

  root /var/www/leht;
  index index.php index.html index.htm;
  charset utf-8;

  location / {
      try_files \$uri \$uri/ /index.html;
  }

  location ~ \.php\$ {
      fastcgi_pass 127.0.0.1:9000;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
      include fastcgi_params;
      include /etc/nginx/fastcgi_params;
  }

  location = /favicon.ico {
      log_not_found off;
      access_log off;
  }

  location = /robots.txt {
      log_not_found off;
      access_log off;
  }

  location ~ /\.(?!well-known) {
      deny all;
  }
}
EOF
                    else
                        cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
listen 80;
listen [::]:80;
server_name $misip;

root /var/www/leht;
index index.php index.html index.htm;
charset utf-8;

location / {
try_files \$uri \$uri/ /index.html;
}

location ~ \.php\$ {
fastcgi_pass unix:/run/php-fpm/www.sock;
fastcgi_index index.php;
fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
include fastcgi_params;
include /etc/nginx/fastcgi_params;
}

location = /favicon.ico {
log_not_found off;
access_log off;
}

location = /robots.txt {
log_not_found off;
access_log off;
}

location ~ /\.(?!well-known) {
deny all;
}
}
EOF
                    fi
                    echo "NGINXi käivitamine!"
                    systemctl restart nginx
                fi

            elif [ "$kasdomeen" = "Domeen" ]; then
                echo "certboti installimine"
                if [ "$versioon" == "7" ]; then
                    yum -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
                    yum -y -q install yum-utils
                    yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
                    yum -y -q install certbot python2-certbot-nginx
                else
                    wget https://dl.eff.org/certbot-auto
                    mv certbot-auto /usr/local/bin/certbot
                    chown root /usr/local/bin/certbot
                    chmod 0755 /usr/local/bin/certbot
                fi
                echo "SSL sertifikaadi genereerimine!"
                systemctl restart nginx
                certbot certonly --nginx --non-interactive --agree-tos --domains ${misdomeen} --register-unsafely-without-email
                echo "SSL sertifikaat genereeritud!"
                systemctl restart nginx
                crontab -l | {
                    cat
                    echo "43 6 * * * certbot renew --post-hook "systemctl reload nginx""
                } | crontab -
                echo "certbot installitud!"
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
  listen 80;
  server_name ${misdomeen} www.${misdomeen};
  return 301 https://\$server_name\$request_uri;
}

server {
  listen 443 ssl http2;
  server_name ${misdomeen};

  root /var/www/leht;
  index index.html index.htm;
  charset utf-8;

  ssl_session_cache shared:SSL:50m;
  ssl_session_timeout 1d;
  ssl_session_tickets off;

  ssl_certificate /etc/letsencrypt/live/${misdomeen}/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/${misdomeen}/privkey.pem;
  ssl_prefer_server_ciphers on;
  ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
  ssl_protocols TLSv1.2;

  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";



  location / {
      try_files \$uri \$uri/ /index.php;
  }

  location = /favicon.ico {
      log_not_found off;
      access_log off;
  }

  location = /robots.txt {
      log_not_found off;
      access_log off;
  }

  location ~ /\.(?!well-known) {
      deny all;
  }
}
EOF
                    echo "NGINXi käivitamine"
                    systemctl restart nginx
                else
                    centosphpinstall
                    if [ "$versioon" = "7" ]; then
                        cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
    listen 80;
    server_name ${misdomeen} www.${misdomeen};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl http2;
    server_name ${misdomeen};

    root /var/www/leht;
    index index.php index.html index.htm;
    charset utf-8;

    ssl_session_cache shared:SSL:50m;
    ssl_session_timeout 1d;
    ssl_session_tickets off;

    ssl_certificate /etc/letsencrypt/live/${misdomeen}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${misdomeen}/privkey.pem;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    ssl_protocols TLSv1.2;

    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";



    location / {
        try_files \$uri \$uri/ /index.php;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        log_not_found off;
        access_log off;
    }

    location ~ \.php\$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
        include /etc/nginx/fastcgi_params;
    }

    location ~ /\.(?!well-known) {
        deny all;
    }
}
EOF
                    else
                        cat <<EOF >/etc/nginx/conf.d/leht.conf
server_tokens off;

server {
listen 80;
server_name ${misdomeen} www.${misdomeen};
return 301 https://\$server_name\$request_uri;
}

server {
listen 443 ssl http2;
server_name ${misdomeen};

root /var/www/leht;
index index.php index.html index.htm;
charset utf-8;

ssl_session_cache shared:SSL:50m;
ssl_session_timeout 1d;
ssl_session_tickets off;

ssl_certificate /etc/letsencrypt/live/${misdomeen}/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/${misdomeen}/privkey.pem;
ssl_prefer_server_ciphers on;
ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
ssl_protocols TLSv1.2;

add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";



location / {
try_files \$uri \$uri/ /index.php;
}

location = /favicon.ico {
log_not_found off;
access_log off;
}

location = /robots.txt {
log_not_found off;
access_log off;
}

location ~ \.php\$ {
fastcgi_pass unix:/run/php-fpm/www.sock;
fastcgi_index index.php;
fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
include fastcgi_params;
include /etc/nginx/fastcgi_params;
}

location ~ /\.(?!well-known) {
deny all;
}
}
EOF
                    fi
                fi
                echo "NGINXi käivitamine!"
                systemctl restart nginx
            fi

        fi
    elif [ "$misveebiserver" = "Apache" ]; then
        mkdir -p /var/www/leht
        if [ "$distro" = "ubuntu" ] || [ "$distro" = "debian" ]; then
            ufw allow 80
            ufw allow 443
            apt -y -qq update >/dev/null
            echo "Apache installimine"
            apt -qq -y install apache2 >/dev/null
            a2enmod ssl
            a2enmod rewrite
            echo "Apache installitud!"
            if [ "$kasdomeen" = "IP" ]; then
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/apache2/sites-available/leht.conf
<VirtualHost *:80>
  ServerName ${misip}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  ErrorLog \${APACHE_LOG_DIR}/leht.error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/apache2/sites-available/leht.conf /etc/apache2/sites-enabled/leht.conf
                    echo "Apache käivitamine!"
                    systemctl restart apache2
                else
                    ubdebphp
                    cat <<EOF >/etc/apache2/sites-available/leht.conf
<VirtualHost *:80>
  ServerName ${misip}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  ErrorLog \${APACHE_LOG_DIR}/leht.error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/apache2/sites-available/leht.conf /etc/apache2/sites-enabled/leht.conf
                    echo "Apache käivitamine!"
                    systemctl restart apache2
                fi

            elif [ "$kasdomeen" = "Domeen" ]; then
                if [ "$distro" = "ubuntu" ]; then
                    echo "certboti installimine"
                    apt -y -qq install software-properties-common >/dev/null
                    add-apt-repository -y universe >/dev/null
                    add-apt-repository -y ppa:certbot/certbot >/dev/null
                    apt -y -qq update >/dev/null
                    apt -y -qq install certbot python-certbot-apache >/dev/null
                    echo "certbot installitud!"
                elif [ "$distro" = "debian" ]; then
                    echo "certboti installimine"
                    apt -y -qq install certbot python-certbot-apache >/dev/null
                    echo "certbot installitud!"
                fi
                echo "SSL sertifikaadi genereerimine!"
                systemctl restart apache2
                certbot certonly --apache --non-interactive --agree-tos --domains ${misdomeen} --register-unsafely-without-email
                echo "SSL sertifikaat genereeritud!"
                systemctl restart apache2
                crontab -l | {
                    cat
                    echo "43 6 * * * certbot renew --post-hook "systemctl reload apache2""
                } | crontab -
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/apache2/sites-available/leht.conf
<VirtualHost *:80>
  ServerName ${misdomeen}
  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^/?(.*) https://%{SERVER_NAME}/\$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
  ServerName ${misdomeen}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  ErrorLog \${APACHE_LOG_DIR}/leht.error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/${misdomeen}/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/${misdomeen}/privkey.pem
</VirtualHost>
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/apache2/sites-available/leht.conf /etc/apache2/sites-enabled/leht.conf
                    echo "Apache käivitamine!"
                    systemctl restart apache2
                else
                    ubdebphp
                    cat <<EOF >/etc/apache2/sites-available/leht.conf
<VirtualHost *:80>
  ServerName ${misdomeen}
  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^/?(.*) https://%{SERVER_NAME}/\$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
  ServerName ${misdomeen}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  ErrorLog \${APACHE_LOG_DIR}/leht.error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/${misdomeen}/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/${misdomeen}/privkey.pem
</VirtualHost>
EOF
                    echo "Config fail kirjutatud!"
                    ln -s /etc/apache2/sites-available/leht.conf /etc/apache2/sites-enabled/leht.conf
                    echo "Apache käivitamine!"
                    systemctl restart apache2
                fi
                systemctl restart apache2
            fi
        elif [ "$distro" = "centos" ]; then
            firewall-cmd --permanent --add-port 80/tcp
            firewall-cmd --permanent --add-port 443/tcp
            firewall-cmd --reload
            chcon -Rt httpd_sys_content_t /var/www/leht
            echo "Apache installimine!"
            yum -y -q -e 0 install httpd
            echo "Apache installitud!"
            if [ "$kasdomeen" = "IP" ]; then
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/httpd/conf.d/leht.conf
<VirtualHost *:80>
  ServerName ${misip}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
</VirtualHost>
EOF
                    echo "Apache käivitamine!"
                    systemctl restart httpd
                else
                    centosphpinstall
                    cat <<EOF >/etc/httpd/conf.d/leht.conf
<VirtualHost *:80>
  ServerName ${misip}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
</VirtualHost>
EOF
                    echo "Apache käivitamine!"
                    systemctl restart httpd
                fi

            elif [ "$kasdomeen" = "Domeen" ]; then
                systemctl restart httpd
                echo "certboti installimine"
                if [ "$versioon" == "7" ]; then
                    yum -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
                    yum -y -q install yum-utils
                    yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
                    yum -y -q install certbot python2-certbot-apache
                else
                    wget https://dl.eff.org/certbot-auto
                    mv certbot-auto /usr/local/bin/certbot
                    chown root /usr/local/bin/certbot
                    chmod 0755 /usr/local/bin/certbot
                fi
                echo "SSL sertifikaadi genereerimine!"
                certbot certonly --webroot -w /var/www/html --non-interactive --agree-tos --domains ${misdomeen} --register-unsafely-without-email
                echo "SSL sertifikaat genereeritud!"
                systemctl restart httpd
                crontab -l | {
                    cat
                    echo "43 6 * * * certbot renew --post-hook "systemctl reload httpd""
                } | crontab -
                echo "certbot installitud!"
                if [ "$misphp" = "EiSooviPHPd" ]; then
                    cat <<EOF >/etc/httpd/conf.d/leht.conf
<VirtualHost *:80>
  ServerName ${misdomeen}
  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^/?(.*) https://%{SERVER_NAME}/\$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
  ServerName ${misdomeen}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/${misdomeen}/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/${misdomeen}/privkey.pem
</VirtualHost>
EOF
                    echo "Apache käivitamine!"
                    systemctl restart httpd
                else
                    centosphpinstall
                    cat <<EOF >/etc/httpd/conf.d/leht.conf
<VirtualHost *:80>
  ServerName ${misdomeen}
  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^/?(.*) https://%{SERVER_NAME}/\$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
  ServerName ${misdomeen}
  DocumentRoot "/var/www/leht"
  AllowEncodedSlashes On
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/${misdomeen}/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/${misdomeen}/privkey.pem
</VirtualHost>
EOF
                fi
                echo "Apache käivitamine!"
                systemctl restart httpd
            fi

        fi

    fi

}

kasdomeen() {
    clear
    kasdomeen="$(dialog --clear --backtitle "
        Linuxi automatiseerimine Bash skriptitega" \
        --title "Veebiserver" \
        --menu "Palun kasutage üles-alla nooli valimiseks!\n\
        Kas kasutada veebiserveri jaoks domeeni või IP aadressi?" 15 100 4 \
        IP "IP, näiteks 192.168.1.108" \
        Domeen "Domeen, näiteks test.eeeeee.com" 3>&1 1>&2 2>&3 3>&-)"
    retval=$?
    getretval
    clear
}

misdns() {
    if [ "$kasdomeen" = "IP" ]; then
        clear
        misip="$(dialog --clear --backtitle "
      Linuxi automatiseerimine Bash skriptitega" \
            --title "Veebiserver" \
            --inputbox "Palun sisestage IP (näiteks 192.168.1.108) veebiserveri jaoks!" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        clear
    elif [ "$kasdomeen" = "Domeen" ]; then
        clear
        misdomeen="$(dialog --clear --backtitle "
      Linuxi automatiseerimine Bash skriptitega" \
            --title "Veebiserver" \
            --inputbox "Palun sisestage domeen (näiteks test.eeeeee.com) veebiserveri jaoks!" 15 100 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        clear
        if [ "$distro" == "centos" ]; then
            yum -y -q -e 0 install bind-utils
        fi
        valineip=$(curl checkip.amazonaws.com)
        dnsip=$(dig +short $misdomeen | awk '{ print ; exit }')
        if [ "$valineip" !== "$dnsip" ]; then
            # kordus=true
            # while $kordus; do
            #     read -p "Tuvastatud väline IP on "$valineip", aga domeeni IP "$dnsip". Kas soovite jätkata? (jah/ei): " -r vastusdns
            #     kordus=false
            #     echo
            #     if [[ $vastusdns =~ ^[jJ*] ]]; then
            #         :
            #     elif [[ $vastusdns =~ ^[eE*] ]]; then
            #         clear
            #         exit 1
            #     else
            #         echo "Sobivad vastusevariandid on "jah" või "ei"."
            #         kordus=true
            #     fi
            # done
            dialog --clear --backtitle "
                Linuxi automatiseerimine Bash skriptitega" \
                --title "Domeen" \
                --yesno "Tuvastatud väline IP on \"$valineip\", aga domeeni IP \"$dnsip\". Kas soovite jätkata?" 15 100

            retval=$?
            getretval

        fi
    fi
}

misveebiserver() {
    clear
    misveebiserver="$(dialog --clear --backtitle "
        Linuxi automatiseerimine Bash skriptitega" \
        --title "Veebiserver" \
        --menu "Palun kasutage üles-alla nooli valimiseks!\n\
        Valige veebiserver!" 15 100 4 \
        NGINX "Installi NGINX" \
        Apache "Installi Apache" 3>&1 1>&2 2>&3 3>&-)"
    retval=$?
    getretval
    clear
}

misphp() {
    clear
    misphp="$(dialog --clear --backtitle "
    Linuxi automatiseerimine Bash skriptitega" \
        --title "PHP" \
        --menu "Palun kasutage üles-alla nooli valimiseks!\n\
    Millist PHP versiooni soovite paigaldada?" 15 100 4 \
        7.4 "Installi PHP 7.4" \
        7.3 "Installi PHP 7.3" \
        7.2 "Installi PHP 7.2" \
        EiSooviPHPd "Ei soovida installida PHP-d" 3>&1 1>&2 2>&3 3>&-)"
    retval=$?
    getretval
    clear
}

missql() {
    clear
    missql="$(dialog --clear --backtitle "
    Linuxi automatiseerimine Bash skriptitega" \
        --title "SQL" \
        --menu "Palun kasutage üles-alla nooli valimiseks!\n\
    Millist SQL andmebaasi soovite paigaldada?" 15 100 4 \
        MariaDB "Installi MariaDB 10.4 (soovitatud)" \
        MySQL "Install MySQL 8.0" \
        EiSooviSQLi "Ei soovi installida SQL andmebaasi" 3>&1 1>&2 2>&3 3>&-)"
    retval=$?
    getretval
    clear
}

kaspma() {
    if [ "$misphp" != "EiSooviPHPd" ] && [ "$missql" != "EiSooviSQLi" ]; then
        clear
        mispma="$(dialog --clear --backtitle "
    Linuxi automatiseerimine Bash skriptitega" \
            --title "PHPMyAdmin" \
            --menu "Palun kasutage üles-alla nooli valimiseks!\n\
    Kas soovite paigaldada PHPMyAdmin veebikeskkonna?" 15 100 4 \
            Jah "Installi PHPMyAdmin" \
            Ei "Ei installi PHPMyAdmin'it" 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
    fi
}

kontroll() {
    if [ -f /srv/webvalmis ]; then
        dialog --clear --backtitle "
      Linuxi automatiseerimine Bash skriptitega" \
            --title "Skript" \
            --yesno "On tuvastatud, et veebiserveri skripti on siin juba kasutatud. Kas soovite jätkata?" 15 100
        retval=$?
        getretval
    fi
}

misturvamine() {
    valik="$(dialog --clear --backtitle "
      Linuxi automatiseerimine Bash skriptitega" \
        --title "TURVAMINE" \
        --menu "Palun kasutage üles-alla nooli valimiseks!\n\
      Valige ülesanne!" 15 100 4 \
        SSHPORT "Vaheta SSH port" \
        SSHVOTMED "Turva süsteem kasutades SSH võtmeid!" \
        Fail2ban "Turva süsteem kasutades Fail2bani" 3>&1 1>&2 2>&3 3>&-)"
    retval=$?
    getretval
    case $valik in
    SSHPORT)
        vahetaport
        ;;
    SSHVOTMED)
        sshvotmed
        ;;
    FAIL2BAN)
        fail2ban
        ;;
    esac
}

ennekoike() {
    if [[ $EUID -ne 0 ]]; then
        echo "Palun käivitage see skript uuesti kasutajana 'root' või kasutage sudo-t"
        exit 1
    fi

    if [ -e /etc/os-release ]; then
        distro=$(awk -F= '$1 == "ID" {print $2}' /etc/os-release | sed 's/"//g')
        versioon=$(awk -F= '$1 == "VERSION_ID" {print $2}' /etc/os-release | sed 's/"//g')
    else
        echo "Faili /etc/os-release ei tuvastatud. On võimalik, et teie distrot ei toetata."
        exit 1
    fi

    kordus=true
    while $kordus; do
        read -p "Tuvastatud distro \"$distro\" ja versioon \"$versioon\". Kas see on õige? (jah/ei): " -r vastus
        kordus=false
        echo
        if [[ $vastus =~ ^[jJ*] ]]; then
            :
        elif [[ $vastus =~ ^[eE*] ]]; then
            clear
            exit 1
        else
            echo "Sobivad vastusevariandid on "jah" või "ei"."
            kordus=true
        fi
    done

    if [ "$distro" = "ubuntu" ] && [ "$versioon" != "18.04" ] && [ "$versioon" != "16.04" ]; then
        echo "Kahjuks Teie Ubuntu versiooni ($versioon) see skript ei toeta. Toetatud on versioonid 16.04 ja 18.04."
        exit 1
    elif [ "$distro" = "centos" ] && [ "$versioon" != "7" ] && [ "$versioon" != "8" ]; then
        echo "Kahjuks Teie CentOSi versiooni ($versioon) see skript ei toeta. Toetatud on versioon 7."
        exit 1
    elif [ "$distro" = "debian" ] && [ "$versioon" != "10" ] && [ "$versioon" != "9" ]; then
        echo "Kahjuks Teie Debiani versiooni ($versioon) see skript ei toeta. Toetatud on versioonid 8 ja 9."
        exit 1
    elif [ "$distro" != "debian" ] && [ "$distro" != "ubuntu" ] && [ "$distro" != "centos" ]; then
        echo "Kahjuks Teie distrot ($distro) see skript ei toeta. Toetatud distrod on Ubuntu, CentOS ja Debian."
        exit 1
    fi

    echo "Installin eeltööks vajalikud programmid."
    if [ "$distro" = "ubuntu" ] || [ "$distro" = "debian" ]; then
        echo "Installin dialog'i"
        apt -qq -y install dialog
        echo "Dialog installitud"
    elif [ "$distro" = "centos" ] && [ "$versioon" = "7" ] || [ "$versioon" = "8" ]; then
        echo "Installin dialog'i"
        yum -q -y install dialog
        echo "Dialog installitud"
    fi

    while true; do
        valik="$(dialog --clear --backtitle "
        Linuxi automatiseerimine Bash skriptitega" \
            --title "MENÜÜ" \
            --menu "Palun kasutage üles-alla nooli valimiseks!\n\
        Valige ülesanne!" 15 100 4 \
            Veebiserver "Installi NGINX või Apache, PHP ja SQL andmebaas" \
            Turvamine "Turva süsteem kasutades Fail2ban'i, SSH võtmeid ja SSH porti muutes!" \
            Valju "Välju skriptist" 3>&1 1>&2 2>&3 3>&-)"
        retval=$?
        getretval
        case $valik in
        Veebiserver)
            kontroll
            misveebiserver
            kasdomeen
            misdns
            misphp
            missql
            kaspma
            installiveebiserver
            installisql
            installipma
            veebiserverlopp
            ;;
        Turvamine)
            misturvamine
            ;;
        Valju)
            clear
            exit 1
            ;;
        esac
    done
}
ennekoike
